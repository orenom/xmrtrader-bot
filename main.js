import config from './config.js';
import cheerio from 'cheerio';
import fs from 'fs';
import path, { dirname } from 'path';
import { fileURLToPath } from 'url';
import schedule from 'node-schedule';
const __dirname = dirname(fileURLToPath(import.meta.url));

// File path to store the latest post title
const latest_post_title_file = path.join(__dirname, 'latestPostTitle.txt');

// Holds the current Access Token. Renews every 24 hour.
let access_token = false;

/**
 * Fetches Monero hashrate and transaction data from bitinfocharts.com/monero.
 * @async
 * @returns {object|false} - An object with Monero hashrate and transaction data
 *                           if successful. Returns false in case of errors.
 */
async function fetchHashrateAndTxs() {
    // Define the mapping between labels and tdid values in the HTML
    const mapping = {
        'Hashrate': 'tdid16',
        'Transactions last 24h': 'tdid3'
    };

    try {
        const response = await fetch('https://bitinfocharts.com/monero/');

        if (!response.ok) {
            console.error('fetchHashrateAndTxs() invalid response:', response);
            return false;
        }

        const html = await response.text();
        const $ = cheerio.load(html);
        const data = {};

        $('#main_body table tr').each((index, row) => {
            const label = $(row).find('td:first-child').text().trim();
            const value = $(row).find(`td#${mapping[label]}`).text().trim();
            if (mapping.hasOwnProperty(label)) {
                data[label] = value;
            }
        });

        return data;
    } catch (error) {
        console.error('fetchHashrateAndTxs() error:', error);
        return false;
    }
}

/**
 * Fetches Monero's price and market data from the CoinGecko API.
 * @async
 * @returns {object|false} - An object with Monero's price and market data if
 *                           successful. Returns false in case of errors.
 */
async function fetchPriceAndMarket() {
    const url = 'https://api.coingecko.com/api/v3/coins/markets?vs_currency=usd&ids=monero&price_change_percentage=1h,24h,7d,14d,30d,200d,1y&locale=en';

    try {
        const response = await fetch(url, {
            method: 'GET',
            headers: {
                'accept': 'application/json'
            }
        });

        if (!response.ok) {
            console.error('fetchPriceAndMarket() invalid response:', response);
            return false;
        }

        return await response.json();
    } catch (error) {
        console.error('fetchPriceAndMarket() error:', error);
        return false;
    }
}

/**
 * Formats a numeric value as a percentage change with two decimal places.
 * Adds a '+' or '-' sign to indicate the direction of change.
 *
 * @param {number} val - The numeric value to be formatted.
 * @returns {string} - A string representing the formatted percentage change.
 */
function formatChange(val) {
    val = parseFloat(val).toFixed(2);
    let sign = '+';
    if (val < 0) {
        sign = '-';
    }
    return `${sign}${val}%`;
}

/**
 * Generates a formatted comment with Monero's price and market data, including
 * percentage changes, transactions and hashrate.
 *
 * @param {object[]} tx_data - An object containing transactions data.
 * @param {object[]} price_data - An object containing price and market data.
 * @returns {string|undefined} - A formatted comment if successful; undefined in
                                 case of errors.
 */
async function generateComment(tx_data, price_data) {
    try {
        const d = price_data[0];
        
        const ath_date = d.ath_date.split('T')[0];
        const ath_change = d.ath_change_percentage.toFixed(2) + '%';
        const price_change24h = formatChange(d.price_change_percentage_24h_in_currency);
        const price_change7d = formatChange(d.price_change_percentage_7d_in_currency);
        const price_change30d = formatChange(d.price_change_percentage_30d_in_currency);
        const price_change1y = formatChange(d.price_change_percentage_1y_in_currency);
        let comment = `
|||
|:-------------------------|:---------------------------------|
| **Price (USD)**          | $${d.current_price}   |
| **High (24h)**           | $${d.high_24h}        |
| **Low (24h)**            | $${d.low_24h}         |
| **Price Change 24h**     | ${price_change24h}    |
| **Price Change 7d**      | ${price_change7d}     |
| **Price Change 30d**     | ${price_change30d}    |
| **Price Change 1y**      | ${price_change1y}     |
| **Market Cap Rank**      | #${d.market_cap_rank} |
| **All-Time High**        | $${d.ath} (${ath_date}, ${ath_change}) |
| **Transactions (24h)**   | ${tx_data['Transactions last 24h']}    |
| **Hashrate**             | ${tx_data['Hashrate']}|


[Automated message](https://codeberg.org/orenom/xmrtrader-bot)
        `;

        return comment;
    } catch (error) {
        console.log('generateComment() error:', error);
    }
}

// Function to read the latest post title from the file.
async function latestCommentedPostTitle() {
    try {
        // Check if the file exists
        if (!fs.existsSync(latest_post_title_file)) {
            // Create an empty file if it doesn't exist
            fs.writeFileSync(latest_post_title_file, '', 'utf-8');
            return '';
        }

        const data = fs.readFileSync(latest_post_title_file, 'utf-8');
        return data.trim();
    } catch (error) {
        console.error(
            'latestCommentedPostTitle(): Error reading latest post title:',
            error
        );
        return '';
    }
}

// Function to write the latest post title to the file.
async function writeLatestPostTitle(title) {
    try {
        fs.writeFileSync(latest_post_title_file, title, 'utf-8');
        return true;
    } catch (error) {
        // Handle errors, e.g., unable to write to the file
        console.error(
            'writeLatestPostTitle(): Error writing latest post title:',
            error
        );
        return false;
    }
}

// Generates a Daily Discussion date format for the current date.
function getCurrentFormattedDate() {
    const currentDate = new Date();
    const day = currentDate.getDate().toString().padStart(2, '0');
    const year = currentDate.getFullYear();
    const monthNames = [
        'January', 'February', 'March', 'April', 'May', 'June', 'July',
        'August', 'September', 'October', 'November', 'December'
    ];
    const monthName = monthNames[currentDate.getMonth()];

    return `${monthName} ${day}, ${year}`;
}

/**
 * Finds and returns the latest Daily Discussion post in the xmrtrader
 * subreddit. Checks if the latest post has already been commented on to avoid
 * duplication.
 *
 * @async
 * @returns {object|false} - Returns the latest Daily Discussion post object if
 *                           found, or false if there are errors or no new post
 *                           is available.
 */
async function findLatestPost() {
    try {
        const latest_title = await latestCommentedPostTitle();
        const todays_title = '[Daily Discussion] ' + getCurrentFormattedDate();
        
        if (latest_title === todays_title) {
            /**
             * No need to do anything, we've already commented into the latest
             * Daily Discussion post.
             */
            console.log('findLatestPost(): Already commented into the latest Daily Discussion.');
            return false;
        } else {
            // Find the post where we need to make a comment.
            const url = `https://www.reddit.com/r/xmrtrader/new.json`;

            const response = await fetch(url, {
                headers: {
                    'User-Agent': config.USER_AGENT,
                },
            });

            if (!response.ok) {
                console.error(`findLatestPost(): HTTP error! Status: ${response.status}`);
                return false;
            }

            const data = await response.json();

            if (!data || !data.data || !data.data.children) {
                console.error('findLatestPost(): No data found in the response.');
                return false;
            }

            const posts = data.data.children;
            
            for (const post of posts) {
                if (post.data.title === todays_title) {
                    return post;
                }
            }
            
            console.log('findLatestPost(): did not find Daily Discussion thread.');
            return false;
        }

    } catch (error) {
        console.error('findLatestPost(): Error fetching or processing data:', error);
        return false;
    }
}

// Gets the Access Token 
async function getAccessToken() {
    console.log('getAccessToken(): Getting access token for Reddit...');
    if (access_token) {
        console.log('getAccessToken(): We already have old access token, using it.');
        return access_token;
    }

    console.log('getAccessToken(): Fetching new access token...');

    const basic_payload = `${config.CLIENT_ID}:${config.CLIENT_SECRET}`;
    const auth_header = `Basic ${Buffer.from(basic_payload).toString('base64')}`;
    const body = `grant_type=password&username=${config.USERNAME}&password=${config.PASSWORD}`;

    const headers = {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': auth_header,
        'User-Agent': config.USER_AGENT,
    };

    try {
        const response = await fetch('https://www.reddit.com/api/v1/access_token', {
            method: 'POST',
            headers: headers,
            body: body,
        });

        if (response.status === 200) {
            const token_data = await response.json();

            if (!token_data.access_token) {
                console.error('getAccessToken(): no access token', token_data);
                return false;
            }

            access_token = token_data.access_token;

            const renew_in = token_data.expires_in || 86400;

            setTimeout(() => {
                // renew 1 hour before expiration
                access_token = false;
                getAccessToken();
            }, (renew_in * 1000) - (1000 * 60 * 60));

            return access_token;
        } else {
            console.error(`getAccessToken(): Received a statusCode ${response.status}`);
            return false;
        }
    } catch (error) {
        console.error('getAccessToken() error:', error);
        return false;
    }
}

/**
 * Posts a comment to the Daily Discussion post using the Reddit API.
 *
 * @param {object} post - The Reddit post object to comment on.
 * @param {string} comment - The comment text to post.
 * @returns {boolean|undefined} - Returns true if the comment is successfully
 *                                posted, false on failure. Returns undefined in
 *                                case of errors.
 */
async function postComment(post, comment) {
    const post_id = post.data.name;
    const url = 'https://oauth.reddit.com/api/comment';

    try {
        console.log('postComment(): Posting a comment...');

        const body = `api_type=json&thing_id=${post_id}&text=${encodeURIComponent(comment)}`;

        const response = await fetch(url, {
            method: 'POST',
            headers: {
                'Authorization': `Bearer ${access_token}`,
                'Content-Type': 'application/x-www-form-urlencoded',
            },
            body: body,
        });

        if (!response.ok) {
            console.error(`postComment(): HTTP error! Status: ${response.status}`);
            return false;
        }

        const responseData = await response.json();

        console.log(
            `postComment(): Commented into a post: ${post_id} - ${post.data.title}`,
            comment
        );

        return true;
    } catch (error) {
        console.error('postComment() error:', error);
        return false;
    }
}

/**
 * Fetches data and posts a comment to the latest Daily Discussion post on
 * /r/xmrtrader subreddit.
 *
 * This function orchestrates the process of fetching various data, generating a
 * comment, and posting it to the latest Daily Discussion post. It also handles
 * errors and writes the title of the latest post into a file for tracking.
 *
 * @async
 * @function fetchDataAndPostAComment
 * @throws {Error} If any step in the process encounters an error.
 */
async function fetchDataAndPostAComment() {
    try {
        await getAccessToken();
        
        const post = await findLatestPost();

        if (!post) {
            console.log('fetchDataAndPostAComment(): Invalid post', new Date());
            return;
        }

        const tx_data = await fetchHashrateAndTxs();
        const price_data = await fetchPriceAndMarket();
        const comment = await generateComment(tx_data, price_data);
        const post_comment = await postComment(post, comment);

        if (post_comment) {
            writeLatestPostTitle(post.data.title);
        } else {
            console.error(
                'fetchDataAndPostAComment(): !post_comment.',
                post_comment,
                new Date(),
            );
        }
    } catch (error) {
        console.error(
            'fetchDataAndPostAComment(): Something went wrong.',
            post_comment,
            new Date(),
        );
    }
}

/**
 * Schedule a recurring job to run at a 8:05:00 UTC timezone every day.
 *
 * This function creates a scheduled job using the `scheduleJob` function from
 * the 'node-schedule' library to run a specified task at a fixed time each day
 * in the UTC timezone. The task includes simply running the
 * `fetchDataAndPostAComment` function.
 *
 * @function scheduleJob
 */
const scheduleJob = () => {
    const rule = new schedule.RecurrenceRule();

    // Set the hour, minute, and time zone for the scheduled job.
    rule.hour = 8;
    rule.minute = 5;
    rule.tz = 'Etc/UTC';

    // Create and schedule the job.
    const job = schedule.scheduleJob(rule, () => {
        console.log('Running a scheduled job at', new Date());
        fetchDataAndPostAComment();
    });
}

console.log('Started xmrtrader-bot at', new Date());
scheduleJob();
