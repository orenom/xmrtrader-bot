# xmrtrader-bot

Posts a comment into a 'Daily Discussion' thread in /r/xmrtrader. The comment includes price/market information (via CoinGecko API), transaction count and hashrate (via bitinfocharts.com).


Comment is posted daily at 08:05:00 UTC, since a new Daily Discussion post is created by the AutoModerator daily at 08:00:00.


Feel free to make pull requests!


Donate: 839Vz7mXyk4NgHHQUFDSwKEwCDcv5a4CiWR9NhbuAyAwg5amerSqUPUPhuoqFw5FueP6cJT4EsfLSJS2ESvNS7u5S9MrjTq
